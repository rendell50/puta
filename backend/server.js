const express = require("express");
const app = express();
const mongoose = require("mongoose");
const config = require("./config.js");
const cors = require('cors');
mongoose.connect("mongodb+srv://rendell50:rendelllayus@batman-jqnr3.mongodb.net/anapp?retryWrites=true&w=majority",{


	useNewUrlParser:true,useUnifiedTopology:true
}).then(()=>{
	console.log("Remote Database Connection Established");
});
app.use(express.urlencoded({extended:false}));
app.use(express.json());
app.use(cors());
app.listen(config.port, ()=>{
	console.log(`Listening on Port ${config.port}`);
});
//asset api
const assets = require("./routes/assets_router");
const requests = require("./routes/requests_router");
app.use('/admin', assets);
app.use('/admin', requests);