import React, {useState} from 'react';

const RequestRow = props =>{

	const request = props.request;

	return(

		<React.Fragment>
		<tr>
			<td>{request.code}</td>
			<td>{request.requestDate}</td>
			<td>{request.assetName}</td>
			<td>{request.quantity}</td>
			<td>{request.requestor}</td>
			<td>{request.status}</td>
			<td>{request.approver}</td>
			<td></td>
			<td></td>

		</tr>
		</React.Fragment>

		)
}

export default RequestRow;