import React from 'react';

import {
	Modal,
	ModalHeader,
	ModalBody,
	Button
} from 'reactstrap';

import {FormInput} from '../../../globalcomponents';

const RequestForm = props =>{
	return(
		<React.Fragment>
		<Modal
		isOpen={props.showForm}
		toggle={props.handleShowForm}
		>
		<ModalHeader
		toggle={props.handleShowForm}
		style={{"backgroundColor":"indigo", "color":"skyblue"}}
		>Add Request</ModalHeader>
		<ModalBody>
		<FormInput
						label={"Code Name"}
						type={"text"}
						name={"code"}
						placeholder={"Enter Code"}
						required={props.codeRequired}
						onChange={props.handleRequestCodeChange}
						/>

						<FormInput
						label={"Quantity"}
						type={"number"}
						name={"quantity"}
						placeholder={"Enter Quantity"}
						required={props.quantityRequired}
						onChange={props.handleRequestQuantityChange}
						/>

						<FormInput
						label={"Asset Name"}
						type={"text"}
						name={"assetName"}
						placeholder={"Enter Asset Name"}
						required={props.assetNameRequired}
						onChange={props.handleRequestAssetNameChange}
						/>

						<FormInput
						label={"Requestor"}
						type={"text"}
						name={"requestor"}
						placeholder={"Requestor Name"}
						required={props.requestorRequired}
						onChange={props.handleRequestRequestorChange}
						/>

						<FormInput
						label={"Approver"}
						type={"text"}
						name={"approver"}
						placeholder={"Approver Name"}
						required={props.approverRequired}
						onChange={props.handleRequestApproverChange}
						/>

						<Button color="warning"
						
						>Add Request</Button>
		</ModalBody>
		</Modal>
		</React.Fragment>
		)
}

export default RequestForm;