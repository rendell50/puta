import React, {useEffect, useState} from 'react';
import {RequestRow} from './components';
import {Loading} from '../../globalcomponents';
import axios from 'axios';

import {
	Button
} from 'reactstrap';


const Requests = () =>{
	const [requests, setRequests] = useState([]);
	const [isLoading, setIsLoading] = useState(true);


		useEffect(()=>{
		
		axios.get('http://localhost:4000/admin/showrequests'
		).then(res=>{
			setRequests(res.data);
			console.log(res.data)
			setIsLoading(false);
		
		});
	},[])


	return(
		<React.Fragment>
			{isLoading ?
			<Loading />
		:
		<React.Fragment>
		<h1 
		className="text-center py-5">Requests</h1>

				<div className="d-flex justify-content-center py-3 pb-4">
					<Button
					color="success"
					>+ Add Request</Button>
		
					
				</div>
			<div className="vh-100">
			<table
				className="table table-striped border col-lg-10 offset-lg-1"
			>
				<thead>
					<tr>
						<th>Request Code</th>
						<th>Date Requested</th>
						<th>Asset Requested</th>
						<th>Quantity</th>
						<th>Requested By</th>
						<th>Status</th>
						<th>Approved By</th>
						<th>Actions</th>
					

					</tr>
				</thead>
				<tbody>
					{requests.map(request=>
						<RequestRow 
							key={request._id}
							request={request}
							

							
						/>
					)}
			
				</tbody>
			</table>

			</div>
			</React.Fragment>
			}
		</React.Fragment>
		)
}

export default Requests;