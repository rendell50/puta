import React, {useEffect, useState} from 'react';
import {AssetRow, AssetForm} from './components';
import {Loading} from '../../globalcomponents';
import {
	Button
} from 'reactstrap';
import moment from 'moment';
import axios from 'axios';
import {ToastContainer, toast} from 'react-toastify';

const invalidStock = () =>{
	toast.error("Invalid Stock")
}

const invalidDescription = () =>{
	toast.error("Invalid Description")
}

const invalidName = () =>{
	toast.error("Invalid Name")
}




const Assets = () => {
	const [assets, setAssets] = useState([]);
	const [isLoading, setIsLoading] = useState(true);
	const [showForm, setShowForm]= useState(false);
	const [nameRequired, setNameRequired] = useState(true);
	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [descriptionRequired, setDescriptionRequired] = useState(true);
	const [category, setCategory] = useState("");
	const [categoryRequired, setCategoryRequired]=useState(true);
	const [stock, setStock]=useState(0);
	const [stockRequired, setStockRequired]=useState(true);
	const [showStock, setShowStock] = useState(true);
	const [editId, setEditId] = useState("");
	const [showCategory, setShowCategory] = useState(true); 
	const [showDescription, setShowDescription] =useState(true);
	const [showName, setShowName] =useState(true);



	const handleRefresh = () => {
		setShowForm(false);
		setName("");
		setCategory("");
		setDescription("");
		setStock(0);
		setNameRequired(true);
		setCategoryRequired(true);
		setDescriptionRequired(true);
		setStockRequired(true);
	}

	useEffect(()=>{
		// fetch('')
		fetch('http://localhost:4000/admin/showassets'
		).then(response=>response.json()
		).then(data=>{
			setAssets(data);
			setIsLoading(false);
		});
	},[])

	const handleShowForm = () =>{
		setShowForm(!showForm)
	}

	const handleAssetNameChange = (e) => {
		if(e.target.value==""){
			setNameRequired(true);
			setName("")
		}else{
			setNameRequired(false)
			setName(e.target.value);
		}
	}

	const handleAssetDescriptionChange = e => {
		if(e.target.value==""){
			setDescriptionRequired(true);
			setDescription("")
		}else{
			setDescriptionRequired(false)
			setDescription(e.target.value);
		}
	}

	const handleAssetCategoryChange = e => {
		if(e.target.value==""){
			setCategoryRequired(true);
			setCategory("")
		}else{
			setCategoryRequired(false)
			setCategory(e.target.value);
		}
	}

	const handleAssetStockChange = e => {
		if(e.target.value==""){
			setStockRequired(true);
			setStock(0)
		}else{
			setStockRequired(false)
			setStock(e.target.value);
		}
	}

	const handleSaveAsset = () => {
		let serialNumber = moment(new Date).format('x');

		axios({
			method: "POST",
			url: "http://localhost:4000/admin/addasset",
			data:{
				name:name,
				description:description,
				category:category,
				stock:parseInt(stock),
				serialNumber:serialNumber
			}
		}).then(res=>{
			// console.log(res.data);
			let newAssets = [...assets];
			newAssets.push(res.data);
			setAssets(newAssets);

		});

		handleRefresh();
	}

	const handleDeleteAsset = assetId => {
		axios({
			method: "DELETE",
			url: "http://localhost:4000/admin/deleteasset/" + assetId
		}).then(res=>{
			let newAssets = assets.filter(asset=>asset._id!=assetId);
			setAssets(newAssets)
		});
	}

	const handleEditStock = (e,assetId) => {
		let stock = e.target.value;
		// console.log(e.target.value)
		if(stock <= 0){
			setShowStock(true);
			invalidStock();
		}else{
			axios({
				method:'PATCH',
				url: 'http://localhost:4000/admin/updatestock/' + assetId,
				data: {
					stock: stock
				}
			}).then(res=>{
				let oldIndex;
				assets.forEach((asset,index)=>{
					if(asset._id === assetId){
						oldIndex=index;
					}
				});

				let newAssets = [...assets];
				newAssets.splice(oldIndex, 1, res.data);
				setAssets(newAssets);
				setShowStock(true);
				setEditId("");
			})
		}
	}

	const handleStockEditInput = editId => {
		setEditId(editId)
		setShowStock(true);
		setShowCategory(false);
		setShowDescription(true);
		setShowName(false);

	
	}
	const handleCategoryEditInput = editId=>{
		setEditId(editId);
		setShowStock(false);
		setShowCategory(true);
		setShowDescription(false);
		setShowName(false);
		
		
	}

	const handleDescriptionEditInput = editId =>{
		setEditId(editId)
		setShowStock(false);
		setShowCategory(false)
		setShowDescription(true);
		setShowName(false);

	}


	const handleNameEditInput = editId =>{
		setEditId(editId)
		setShowStock(false);
		setShowCategory(false)
		setShowDescription(false);
		setShowName(true);
	}

	const handleEditCategory = category=>{
		axios({
			method: "PATCH",
			url: "http://localhost:4000/admin/updatecategory/"+editId,
			data:{
				category: category
			}
		}).then(res=>{
			let oldIndex;
				assets.forEach((asset,index)=>{
					if(asset._id === editId){
						oldIndex=index;
					}
				});

				let newAssets = [...assets];
				newAssets.splice(oldIndex, 1, res.data);
				setAssets(newAssets);
				setShowStock(true);
				setEditId("");


		})
	}

	
	const handleEditDescription = (e, assetId)=>{
		let description = e.target.value;
		if(description == ""){
			setShowDescription(true);
			invalidDescription();
		}else{
			axios({
				method: 'PATCH',
				url: "http://localhost:4000/admin/updatedescription/" + assetId,
				data: {
					description: description
				}
			}).then(res=>{
				let oldIndex;
				assets.forEach((asset,index)=>{
					if(asset._id === assetId){
						oldIndex=index;
					}
				});

				let newAssets = [...assets];
				newAssets.splice(oldIndex, 1, res.data);
				setAssets(newAssets);
				setShowDescription(true);
				setEditId("");

			})
		}
	}




		const handleEditName = (e, assetId)=>{
		let name = e.target.value;
		if(name == ""){
			setShowName(true);
			invalidName();
		}else{
			axios({
				method: 'PATCH',
				url: "http://localhost:4000/admin/updatename/" + assetId,
				data: {
					name: name
				}
			}).then(res=>{
				let oldIndex;
				assets.forEach((asset,index)=>{
					if(asset._id === assetId){
						oldIndex=index;
					}
				});

				let newAssets = [...assets];
				newAssets.splice(oldIndex, 1, res.data);
				setAssets(newAssets);
				setShowName(true);
				setEditId("");

			})
		}
	}


	return (
		<React.Fragment>
		<ToastContainer />
		{isLoading ?
			<Loading />
		:
		<React.Fragment>
			<div>
				<h1 
				className="text-center py-5">Assets</h1>
				<div className="d-flex justify-content-center py-3 pb-4">
					<Button
					color="success"
					onClick={handleShowForm}
					>+ Add Asset</Button>
					<AssetForm 
						showForm={showForm}
						handleShowForm={handleShowForm}
						handleAssetNameChange={handleAssetNameChange}
						nameRequired={nameRequired}
						handleAssetDescriptionChange={handleAssetDescriptionChange}
						descriptionRequired={descriptionRequired}
						handleAssetCategoryChange={handleAssetCategoryChange}
						categoryRequired={categoryRequired}
						handleAssetStockChange={handleAssetStockChange}
						stockRequired={stockRequired}
						name={name}
						description={description}
						category={category}
						stock={stock}
						handleSaveAsset={handleSaveAsset}
					/>
				</div>
			</div>
			<div className="vh-100">
			<table
				className="table table-striped border col-lg-10 offset-lg-1"
			>
				<thead>
					<tr>
						<th>Serial No.</th>
						<th>Name</th>
						<th>Description</th>
						<th>Category</th>
						<th>Stock</th>
						<th>Actions</th>
					</tr>
				</thead>
				<tbody>
					{assets.map(asset=>
						<AssetRow 
							key={asset._id}
							asset={asset}
							handleDeleteAsset={handleDeleteAsset}
							setShowStock={setShowStock}
							showStock={showStock}
							handleEditStock={handleEditStock}
							handleStockEditInput={handleStockEditInput}
							editId={editId}
							handleCategoryEditInput={handleCategoryEditInput}
							showCategory={showCategory}
							handleEditCategory={handleEditCategory}
							handleDescriptionEditInput={handleDescriptionEditInput}
							setShowDescription={setShowDescription}
							handleEditDescription={handleEditDescription}
							showDescription={showDescription}


							handleNameEditInput={handleNameEditInput}
							setShowName={setShowName}
							handleEditName={handleEditName}
							showName={showName}

							
						/>
					)}
				</tbody>
			</table>

			</div>
		</React.Fragment>
		}
		</React.Fragment>
	)
}

export default Assets;